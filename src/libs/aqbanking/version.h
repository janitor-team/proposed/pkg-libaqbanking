/***************************************************************************
 begin       : Mon Mar 01 2004
 copyright   : (C) 2004 by Martin Preuss
 email       : martin at libchipcard.de

 ***************************************************************************
 *          Please see toplevel file COPYING for license details           *
 ***************************************************************************/


#ifndef AQBANKING_VERSION_H
#define AQBANKING_VERSION_H

#define AQBANKING_VERSION_MAJOR 6
#define AQBANKING_VERSION_MINOR 3
#define AQBANKING_VERSION_PATCHLEVEL 2
#define AQBANKING_VERSION_BUILD 0
#define AQBANKING_VERSION_TAG "stable"
#define AQBANKING_VERSION_FULL_STRING "6.3.2.0stable"
#define AQBANKING_VERSION_STRING "6.3.2"


#define AQBANKING_SO_CURRENT 47
#define AQBANKING_SO_REVISION 2
#define AQBANKING_SO_AGE 3
#define AQBANKING_SO_EFFECTIVE 44

#endif /* AQBANKING_VERSION_H */


