/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "bpdjob.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "./bpdjob_p.h"

#include <gwenhywfar/misc.h>
#include <gwenhywfar/debug.h>

/* code headers */
#include <string.h>

/* macro functions */
GWEN_LIST_FUNCTIONS(AQFINTS_BPDJOB, AQFINTS_BpdJob)


AQFINTS_BPDJOB *AQFINTS_BpdJob_new(void) {
  AQFINTS_BPDJOB *p_struct;

  GWEN_NEW_OBJECT(AQFINTS_BPDJOB, p_struct)
  p_struct->_refCount=1;
  GWEN_LIST_INIT(AQFINTS_BPDJOB, p_struct)
  /* members */
  p_struct->flags=0;
  p_struct->name=NULL;
  p_struct->code=NULL;
  p_struct->version=0;
  p_struct->jobsPerMsg=1;
  p_struct->minSigs=0;
  p_struct->securityClass=0;
  p_struct->settings=NULL;
  p_struct->runtimeFlags=0;
  /* virtual functions */

  return p_struct;
}

void AQFINTS_BpdJob_free(AQFINTS_BPDJOB *p_struct) {
  if (p_struct) {
  assert(p_struct->_refCount);
  if (p_struct->_refCount==1) {
    GWEN_LIST_FINI(AQFINTS_BPDJOB, p_struct)
  /* members */
    free(p_struct->name); p_struct->name=NULL;
    free(p_struct->code); p_struct->code=NULL;
    GWEN_DB_Group_free(p_struct->settings); p_struct->settings=NULL;
    p_struct->_refCount=0;
    GWEN_FREE_OBJECT(p_struct);
  }
  else
    p_struct->_refCount--;
  }
}

void AQFINTS_BpdJob_Attach(AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  assert(p_struct->_refCount);
  p_struct->_refCount++;
}

AQFINTS_BPDJOB *AQFINTS_BpdJob_dup(const AQFINTS_BPDJOB *p_src) {
  AQFINTS_BPDJOB *p_struct;

  assert(p_src);
  p_struct=AQFINTS_BpdJob_new();
  /* member "flags" */
  p_struct->flags=p_src->flags;

  /* member "name" */
  if (p_struct->name) {
    free(p_struct->name); p_struct->name=NULL;
    p_struct->name=NULL;
  }
  if (p_src->name) {
    p_struct->name=strdup(p_src->name);
  }

  /* member "code" */
  if (p_struct->code) {
    free(p_struct->code); p_struct->code=NULL;
    p_struct->code=NULL;
  }
  if (p_src->code) {
    p_struct->code=strdup(p_src->code);
  }

  /* member "version" */
  p_struct->version=p_src->version;

  /* member "jobsPerMsg" */
  p_struct->jobsPerMsg=p_src->jobsPerMsg;

  /* member "minSigs" */
  p_struct->minSigs=p_src->minSigs;

  /* member "securityClass" */
  p_struct->securityClass=p_src->securityClass;

  /* member "settings" */
  if (p_struct->settings) {
    GWEN_DB_Group_free(p_struct->settings); p_struct->settings=NULL;
    p_struct->settings=NULL;
  }
  if (p_src->settings) {
    p_struct->settings=GWEN_DB_Group_dup(p_src->settings);
  }

  /* member "runtimeFlags" */
  p_struct->runtimeFlags=p_src->runtimeFlags;

  return p_struct;
}

AQFINTS_BPDJOB *AQFINTS_BpdJob_copy(AQFINTS_BPDJOB *p_struct, const AQFINTS_BPDJOB *p_src) {
    assert(p_struct);
  assert(p_src);
  /* member "flags" */
  p_struct->flags=p_src->flags;

  /* member "name" */
  if (p_struct->name) {
    free(p_struct->name); p_struct->name=NULL;
    p_struct->name=NULL;
  }
  if (p_src->name) {
    p_struct->name=strdup(p_src->name);
  }

  /* member "code" */
  if (p_struct->code) {
    free(p_struct->code); p_struct->code=NULL;
    p_struct->code=NULL;
  }
  if (p_src->code) {
    p_struct->code=strdup(p_src->code);
  }

  /* member "version" */
  p_struct->version=p_src->version;

  /* member "jobsPerMsg" */
  p_struct->jobsPerMsg=p_src->jobsPerMsg;

  /* member "minSigs" */
  p_struct->minSigs=p_src->minSigs;

  /* member "securityClass" */
  p_struct->securityClass=p_src->securityClass;

  /* member "settings" */
  if (p_struct->settings) {
    GWEN_DB_Group_free(p_struct->settings); p_struct->settings=NULL;
    p_struct->settings=NULL;
  }
  if (p_src->settings) {
    p_struct->settings=GWEN_DB_Group_dup(p_src->settings);
  }

  /* member "runtimeFlags" */
  p_struct->runtimeFlags=p_src->runtimeFlags;

  return p_struct;
}

uint32_t AQFINTS_BpdJob_GetFlags(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->flags;
}

const char *AQFINTS_BpdJob_GetName(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->name;
}

const char *AQFINTS_BpdJob_GetCode(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->code;
}

int AQFINTS_BpdJob_GetVersion(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->version;
}

int AQFINTS_BpdJob_GetJobsPerMsg(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->jobsPerMsg;
}

int AQFINTS_BpdJob_GetMinSigs(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->minSigs;
}

int AQFINTS_BpdJob_GetSecurityClass(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->securityClass;
}

GWEN_DB_NODE *AQFINTS_BpdJob_GetSettings(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->settings;
}

uint32_t AQFINTS_BpdJob_GetRuntimeFlags(const AQFINTS_BPDJOB *p_struct) {
  assert(p_struct);
  return p_struct->runtimeFlags;
}

void AQFINTS_BpdJob_SetFlags(AQFINTS_BPDJOB *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->flags=p_src;
}

void AQFINTS_BpdJob_AddFlags(AQFINTS_BPDJOB *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->flags|=p_src;
}

void AQFINTS_BpdJob_SubFlags(AQFINTS_BPDJOB *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->flags&=~p_src;
}

void AQFINTS_BpdJob_SetName(AQFINTS_BPDJOB *p_struct, const char *p_src) {
  assert(p_struct);
  if (p_struct->name) {
    free(p_struct->name); p_struct->name=NULL;
  }
  if (p_src) {
    p_struct->name=strdup(p_src);
  }
  else {
    p_struct->name=NULL;
  }
}

void AQFINTS_BpdJob_SetCode(AQFINTS_BPDJOB *p_struct, const char *p_src) {
  assert(p_struct);
  if (p_struct->code) {
    free(p_struct->code); p_struct->code=NULL;
  }
  if (p_src) {
    p_struct->code=strdup(p_src);
  }
  else {
    p_struct->code=NULL;
  }
}

void AQFINTS_BpdJob_SetVersion(AQFINTS_BPDJOB *p_struct, int p_src) {
  assert(p_struct);
  p_struct->version=p_src;
}

void AQFINTS_BpdJob_SetJobsPerMsg(AQFINTS_BPDJOB *p_struct, int p_src) {
  assert(p_struct);
  p_struct->jobsPerMsg=p_src;
}

void AQFINTS_BpdJob_SetMinSigs(AQFINTS_BPDJOB *p_struct, int p_src) {
  assert(p_struct);
  p_struct->minSigs=p_src;
}

void AQFINTS_BpdJob_SetSecurityClass(AQFINTS_BPDJOB *p_struct, int p_src) {
  assert(p_struct);
  p_struct->securityClass=p_src;
}

void AQFINTS_BpdJob_SetSettings(AQFINTS_BPDJOB *p_struct, GWEN_DB_NODE *p_src) {
  assert(p_struct);
  if (p_struct->settings) {
    GWEN_DB_Group_free(p_struct->settings); p_struct->settings=NULL;
  }
  p_struct->settings=p_src;
}

void AQFINTS_BpdJob_SetRuntimeFlags(AQFINTS_BPDJOB *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->runtimeFlags=p_src;
}

void AQFINTS_BpdJob_AddRuntimeFlags(AQFINTS_BPDJOB *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->runtimeFlags|=p_src;
}

void AQFINTS_BpdJob_SubRuntimeFlags(AQFINTS_BPDJOB *p_struct, uint32_t p_src) {
  assert(p_struct);
  p_struct->runtimeFlags&=~p_src;
}

/* list1 functions */
AQFINTS_BPDJOB_LIST *AQFINTS_BpdJob_List_dup(const AQFINTS_BPDJOB_LIST *p_src) {
  AQFINTS_BPDJOB_LIST *p_dest;
  AQFINTS_BPDJOB *p_elem;

  assert(p_src);
  p_dest=AQFINTS_BpdJob_List_new();
  p_elem=AQFINTS_BpdJob_List_First(p_src);
  while(p_elem) {
    AQFINTS_BPDJOB *p_cpy;

    p_cpy=AQFINTS_BpdJob_dup(p_elem);
    AQFINTS_BpdJob_List_Add(p_cpy, p_dest);
    p_elem=AQFINTS_BpdJob_List_Next(p_elem);
  }

  return p_dest;
}

void AQFINTS_BpdJob_ReadDb(AQFINTS_BPDJOB *p_struct, GWEN_DB_NODE *p_db) {
  assert(p_struct);
  /* member "flags" */
  p_struct->flags=GWEN_DB_GetIntValue(p_db, "flags", 0, 0);

  /* member "name" */
  if (p_struct->name) {
    free(p_struct->name); p_struct->name=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "name", 0, NULL); if (s) p_struct->name=strdup(s); }
  if (p_struct->name==NULL) {  p_struct->name=NULL;
  }

  /* member "code" */
  if (p_struct->code) {
    free(p_struct->code); p_struct->code=NULL;
  }
  { const char *s; s=GWEN_DB_GetCharValue(p_db, "code", 0, NULL); if (s) p_struct->code=strdup(s); }
  if (p_struct->code==NULL) {  p_struct->code=NULL;
  }

  /* member "version" */
  p_struct->version=GWEN_DB_GetIntValue(p_db, "version", 0, 0);

  /* member "jobsPerMsg" */
  p_struct->jobsPerMsg=GWEN_DB_GetIntValue(p_db, "jobsPerMsg", 0, 1);

  /* member "minSigs" */
  p_struct->minSigs=GWEN_DB_GetIntValue(p_db, "minSigs", 0, 0);

  /* member "securityClass" */
  p_struct->securityClass=GWEN_DB_GetIntValue(p_db, "securityClass", 0, 0);

  /* member "settings" */
  if (p_struct->settings) {
    GWEN_DB_Group_free(p_struct->settings); p_struct->settings=NULL;
  }
  { GWEN_DB_NODE *dbSrc; dbSrc=GWEN_DB_GetGroup(p_db, GWEN_PATH_FLAGS_NAMEMUSTEXIST, "settings"); if (dbSrc) { p_struct->settings=GWEN_DB_Group_dup(dbSrc); } else p_struct->settings=NULL; }
  if (p_struct->settings==NULL) {  p_struct->settings=NULL;
  }

  /* member "runtimeFlags" */
  /* member "runtimeFlags" is volatile, just presetting */
  p_struct->runtimeFlags=0;

}

int AQFINTS_BpdJob_WriteDb(const AQFINTS_BPDJOB *p_struct, GWEN_DB_NODE *p_db) {
  int p_rv;

  assert(p_struct);
  /* member "flags" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "flags", p_struct->flags);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "name" */
  if (p_struct->name) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "name", p_struct->name); else { GWEN_DB_DeleteVar(p_db, "name"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "code" */
  if (p_struct->code) p_rv=GWEN_DB_SetCharValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "code", p_struct->code); else { GWEN_DB_DeleteVar(p_db, "code"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "version" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "version", p_struct->version);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "jobsPerMsg" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "jobsPerMsg", p_struct->jobsPerMsg);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "minSigs" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "minSigs", p_struct->minSigs);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "securityClass" */
  p_rv=GWEN_DB_SetIntValue(p_db, GWEN_DB_FLAGS_OVERWRITE_VARS, "securityClass", p_struct->securityClass);
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "settings" */
  if (p_struct->settings){ GWEN_DB_NODE *dbCopy; dbCopy=GWEN_DB_GetGroup(p_db, GWEN_DB_FLAGS_DEFAULT, "settings"); assert(dbCopy); p_rv=GWEN_DB_AddGroupChildren(dbCopy, p_struct->settings); } else { GWEN_DB_DeleteGroup(p_db, "settings"); p_rv=0; }
  if (p_rv<0) {
    DBG_INFO(GWEN_LOGDOMAIN, "here (%d)\n", p_rv);
    return p_rv;
  }

  /* member "runtimeFlags" is volatile, not writing to db */

  return 0;
}

AQFINTS_BPDJOB *AQFINTS_BpdJob_fromDb(GWEN_DB_NODE *p_db) {
  AQFINTS_BPDJOB *p_struct;
  p_struct=AQFINTS_BpdJob_new();
  AQFINTS_BpdJob_ReadDb(p_struct, p_db);
  return p_struct;
}

int AQFINTS_BpdJob_toDb(const AQFINTS_BPDJOB *p_struct, GWEN_DB_NODE *p_db) {
  return AQFINTS_BpdJob_WriteDb(p_struct, p_db);
}


/* code headers */

