AQHBCI-TOOL4(1)
==============
:author:  Micha Lenk
:email:   micha@debian.org
:revdate: 2020-03-21

NAME
----
aqhbci-tool4 - Command line tools for setup, modification and debugging of HBCI settings.

SYNOPSIS
--------
'aqhbci-tool4' [global options] <command> [command options]

DESCRIPTION
-----------
The aqhbci-tool4(1) command can be used to setup and manage AqBanking's HBCI users, customers and accounts.

OPTIONS
-------
Two common options need to be distinguished carefully from each
other: "-c CUSTOMER_ID" refers to the German "Kunden-ID" or
"Kundennummer". "-u USER_ID" refers to the German
"Benutzerkennung". If your bank has specified both to you, you
need to check carefully not to confuse one with the other.

include::aqhbci-tool4.1.generated.txt[]

For help on options available to specific commands, run aqhbci-tool4 <COMMAND> --help.

FILES
-----
 $HOME/.aqbanking/::
    All files in this directory are part of the AqBanking configuration.
    Even though mostly all files are in a simple plaintext syntax, you
    MUST NOT change anything in here without using the tools or libraries
    provided by AqBanking, except you know exactly what you are doing.
    Those files may be subject to changes in future AqBanking releases.

BUGS
----
Probably lots, please post them to the mailing list (see Resources below)
or file an issue in the https://www.aquamaniac.de/rdm/projects/aqbanking/issues[issue tracker]
when you find them.

RESOURCES
---------
The https://www.aquamaniac.de/rdm/projects/aqbanking/wiki[AqBanking wiki]
provides useful help for setting up AqBanking, even though it is only available
in German.
On the https://mailman.aqbanking.de/listinfo/aqbanking-user[AqBanking mailing lists]
you can give kudos to the developers or get your questions answered.

COPYING
-------
Copyright \(C) Martin Preuß.
Free use of this software is granted under the terms of the GNU General
Public License (GPL).

The manpage was composed by Micha Lenk and converted by the nifty tool
AsciiDoc into a manpage.
