#! /bin/sh

echoexec() {
  echo $@
  $@
}

compile_execute_test() {
  local name=${0##*/}
  local package="$@"

  CFLAGS=$(pkg-config --cflags $package)
  LIBS=$(pkg-config --libs $package)

  echo "Using CFLAGS: $CFLAGS"
  echo "Using LIBS:   $LIBS"
  echo
  echo "Compiling ${name}.c"
  echoexec gcc -c -o $ADTTMP/${name}.o debian/tests/src/${name}.c $CFLAGS
  echo
  echo "Linking ${name}"
  echoexec gcc -o $ADTTMP/$name $ADTTMP/${name}.o $LIBS
  echo
  set -x
  mkdir -pv $ADTTMP/home
  export HOME=$ADTTMP/home
  chmod a+x $ADTTMP/$name
  set +x
  echo "Execute built binary: ${name}"
  echoexec $ADTTMP/$name
}
